const btns = document.querySelectorAll("button");
const wrapper = document.querySelector(".wrapper");

console.log(btns[0].classList.length);
console.log(btns[0].classList.item(1));
console.log(btns[0].classList.add("red", "additional"));
console.log(btns[0].classList.remove("blue"));
console.log(btns[0].classList.toggle("blue"));

btns[1].classList.add("red");
if (btns[1].classList.contains("red")) {
  console.log("has red");
}

btns[0].addEventListener("click", () => {
  //   if (!btns[1].classList.contains("red")) {
  //     btns[1].classList.add("red");
  //   } else {
  //     btns[1].classList.remove("red");
  //   }
  btns[1].classList.toggle("red");
});

wrapper.addEventListener("click", (event) => {
  if (event.target && event.target.matches("button.red")) {
    console.log("Hello");
  }
});

const newBtn = document.createElement("button");
newBtn.classList.add("red");
wrapper.appendChild(newBtn);
