window.addEventListener("DOMContentLoaded", () => {
  /* Tabs */

  const tabs = document.querySelectorAll(".tabheader__item");
  const tabsContent = document.querySelectorAll(".tabcontent");
  const tabsParent = document.querySelector(".tabheader__items");

  function hideTabContent() {
    tabsContent.forEach((item) => {
      item.classList.add("hide");
      item.classList.remove("show", "fade");
    });

    tabs.forEach((tab) => {
      tab.classList.remove("tabheader__item_active");
    });
  }

  function showTabContent(i = 0) {
    tabsContent[i].classList.add("show", "fade");
    tabsContent[i].classList.remove("hide");
    tabs[i].classList.add("tabheader__item_active");
  }

  hideTabContent();
  showTabContent();

  tabsParent.addEventListener("click", (event) => {
    const target = event.target;

    if (target && target.classList.contains("tabheader__item")) {
      tabs.forEach((item, i) => {
        if (target == item) {
          hideTabContent();
          showTabContent(i);
        }
      });
    }
  });

  /* Timer */
  const deadline = "2023-08-20::18:00";

  function getTimeRemaining(endtime) {
    const t = Date.parse(endtime) - Date.parse(new Date());
    const days = Math.floor(t / (1000 * 60 * 60 * 24));
    const hours = Math.floor((t / (1000 * 60 * 60)) % 24);
    const minutes = Math.floor((t / (1000 * 60)) % 60);
    const seconds = Math.floor((t / 1000) % 60);

    return { total: t, days, hours, minutes, seconds };
  }

  function getZero(num) {
    if (num >= 0 && num < 10) {
      return `0${num}`;
    } else {
      return num;
    }
  }
  function setClock(endtime, selector = ".timer") {
    const timer = document.querySelector(selector);
    const days = document.querySelector("#days");
    const hours = document.querySelector("#hours");
    const minutes = document.querySelector("#minutes");
    const seconds = document.querySelector("#seconds");

    const timeInterval = setInterval(updateCloak, 1000);
    updateCloak();

    function updateCloak() {
      const t = getTimeRemaining(endtime);

      days.textContent = getZero(t.days);
      hours.textContent = getZero(t.hours);
      minutes.textContent = getZero(t.minutes);
      seconds.textContent = getZero(t.seconds);

      if (t.total <= 0) {
        clearInterval(timeInterval);
      }
    }
  }

  setClock(deadline);

  /* Modal */

  const modalBtns = document.querySelectorAll("[data-modal]");
  const modal = document.querySelector(".modal");
  const modalCloseBnt = document.querySelector("[data-modal-close-btn]");
  const modalDialog = modal.querySelector(".modal-dialog");

  function openModal() {
    modal.classList.add("show");
    document.body.style.overflow = "hidden";

    document.addEventListener("keydown", (event) => {
      if (event.code === "Escape") {
        closeModal();
      }
    });
  }
  function closeModal() {
    modal.classList.remove("show");
    document.body.style.overflow = "";
  }

  modalBtns.forEach((btn) => {
    btn.addEventListener("click", (event) => {
      openModal();
    });
  });

  modalCloseBnt.addEventListener("click", (event) => {
    closeModal();
  });
  modal.addEventListener("click", (event) => {
    if (event.target === modal) {
      closeModal();
    }
  });
});
